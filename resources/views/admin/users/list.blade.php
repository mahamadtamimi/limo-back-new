@extends('admin.main')

@section('content')

    @if ($errors->any())

{{--        <x-admin.alarm.error :massage="$errors->all()"/>--}}

    @endif

    @if (session('success'))
{{--        <x-admin.alarm.success :massage="session('success')"></x-admin.alarm.success>--}}
    @endif





    <div class="bg-white dark:bg-gray-800 overflow-hidden shadow-sm sm:rounded-lg">
        <div class="rounded-t-xl overflow-hidden  p-10">
            <table class="table-auto w-full">
                <thead>
                <tr>

                    <th class="px-4 py-2 text-gray-200 font-bold">@lang('user.name')</th>

                    <th class="px-4 py-2 text-gray-200 font-bold">@lang('user.role')</th>


                    <th class="px-4 py-2 text-emerald-600"></th>
                </tr>
                </thead>
                <tbody>
                @forelse($users as $user)
                    <tr>
                        <td class="border text-gray-200 border-gray-600 px-8 py-2 font-medium divide-y divide-gray-600">
                            <span class="block py-3">
                                {{$user->name}}
                            </span>
                            <span class="block py-3">
                                {{$user->email}}
                            </span>
                        </td>
                        <td class="border text-gray-200 border-gray-600 px-8 py-2 font-medium divide-y divide-gray-600">
                            <span>
{{--                                @foreach($user->role as $role)--}}

{{--                                    {{$role->farsi_name}}--}}
{{--                                @endforeach--}}
                            </span>
                        </td>

                        <td class="border border-gray-600 px-4 py-2 text-emerald-600 font-medium text-center">


                            <a href=""
                               class="dark:bg-gray  mb-10 block text-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                     fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round"
                                     stroke-linejoin="round" class="feather feather-edit-3 inline-block">
                                    <path d="M12 20h9"></path>
                                    <path d="M16.5 3.5a2.121 2.121 0 0 1 3 3L7 19l-4 1 1-4L16.5 3.5z"></path>
                                </svg>
                            </a>

                            <a href=""

                               class="bg-gray block text-center delete-box">

                                <svg class="fill-amber-700 inline-block" xmlns="http://www.w3.org/2000/svg"
                                     viewBox="0 0 24 24"
                                     id="IconChangeColor"
                                     height="24" width="24">
                                    <g>
                                        <path fill="none" d="M0 0h24v24H0z" id="mainIconPathAttribute"></path>
                                        <path
                                            d="M4 8h16v13a1 1 0 0 1-1 1H5a1 1 0 0 1-1-1V8zm2 2v10h12V10H6zm3 2h2v6H9v-6zm4 0h2v6h-2v-6zM7 5V3a1 1 0 0 1 1-1h8a1 1 0 0 1 1 1v2h5v2H2V5h5zm2-1v1h6V4H9z"
                                            id="mainIconPathAttribute"></path>
                                    </g>
                                </svg>

                            </a>


                        </td>
                    </tr>
                @empty

                @endforelse
                </tbody>
            </table>
            <div class="mt-3 ltr">
                {{--                {{ $movies->links()  }}--}}

            </div>
        </div>
    </div>


    <script type="module">
        const checkbox = document.querySelector(".delete-box");

        checkbox.addEventListener("click", checkboxClick, false);

        function checkboxClick(event) {
            event.preventDefault();
            alert('ok')

            if (event.target.tagName === 'a') {
                if (confirm("are you sure ?") === true) {
                    window.location = (event.target.parentElement.href)
                }
            }

        }


    </script>
@endsection

