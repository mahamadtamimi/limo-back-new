<?php


use App\Http\Controllers\Api\PaymentController;
use Illuminate\Support\Facades\Route;

Route::prefix('/api/v1/payment')->group(function () {
    Route::post('/create-payment', [PaymentController::class, 'purchasePlan']);
    Route::post('/verify-payment', [PaymentController::class, 'verifyPayment']);

});

