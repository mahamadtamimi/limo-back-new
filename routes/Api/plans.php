<?php

use App\Http\Controllers\Api\PlansController;
use Illuminate\Support\Facades\Route;

Route::prefix('/api/v1/plans')->group(function () {
    Route::get('/list', [PlansController::class , 'list']);
    Route::get('/{id}/show', [PlansController::class , 'show']);
    Route::get('/my-plans', [PlansController::class , 'myPlans']);



});
