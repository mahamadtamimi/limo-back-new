<?php

use Illuminate\Support\Facades\Route;

Route::prefix('/api/v1/auth')->group(function () {
    Route::post('/validate-code', [\App\Http\Controllers\Api\AuthController::class , 'validateCode']);
    Route::post('/get-code', [\App\Http\Controllers\Api\AuthController::class, 'getCode']);
    Route::post('/login', [\App\Http\Controllers\Api\AuthController::class , 'login']);
    Route::post('/register', [\App\Http\Controllers\Api\AuthController::class , 'register']);
    Route::get('/me', [\App\Http\Controllers\Api\AuthController::class, 'me']);

});
