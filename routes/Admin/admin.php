<?php


use Illuminate\Support\Facades\Route;

Route::prefix('/admin')->middleware('auth')->group(function () {

    Route::get('/', function () {
        return view('admin.main');
    });


    Route::get('/users' , function (){
        $users = \App\Models\User::all();
        return view('admin.users.list' , compact('users'));
    })->name('user.list');
});
