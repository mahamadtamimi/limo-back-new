<?php

namespace Database\Seeders;

use App\Models\Plan;
use App\Models\User;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // User::factory(10)->create();

        \App\Models\User::factory()->create([
            'name' => 'Tamimi',
            'email' => 'afshin.hita.h@gmail.com',
            'password' => Hash::make('tamimi27101373'),
        ]);
        Plan::factory()->create([
            'name' => 'Test Plan',
            'price' => 10000,
            'expire_day' => 60
        ]);
    }
}
