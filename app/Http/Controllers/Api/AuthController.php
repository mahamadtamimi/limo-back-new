<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function login(Request $request): \Illuminate\Http\JsonResponse
    {
        $user = User::where('phone', $request->phoneNumber)->firstOrFail();
        return response()->json(['success' => true, 'user' => $user]);
    }

    public function register(Request $request): \Illuminate\Http\JsonResponse
    {

        $user = User::where('phone', $request->phoneNumber)->count();
        if ($user) return response()->json('phone number already exists', 302);
        $userNew = User::create([
            'name' => $request->name,
            'phone' => $request->phoneNumber,
        ]);
        return response()->json(['success' => true, 'user' => $userNew]);
    }


    public function getCode(Request $request): \Illuminate\Http\JsonResponse
    {


        $user = User::where('phone', $request->phoneNumber)->firstOrFail();

        $user->Otp()->create(['otp' => '123456']);

        return response()->json(['success' => true, 'user' => $user]);
    }

    public function validateCode(Request $request): \Illuminate\Http\JsonResponse
    {


        $user = User::where('phone', $request->phoneNumber)->first();

//        return response()->json(['success' => false, 'user' => $user]);

        if ($request->code == '123456') {


            $token = auth('api')->login($user);

            return response()->json(['success' => true, 'token' => $token]);


        }


        return response()->json(['success' => false, 'message' => 'Invalid code']);


    }

    public function me(Request $request): \Illuminate\Http\JsonResponse
    {
        if (!$user = auth('api')->user()) {
            return response()->json(['success' => false, 'message' => 'User not found']);

        } else {
            return response()->json(['success' => true, 'user' => auth('api')->user(), 'token' => $user]);

        };

    }
}
