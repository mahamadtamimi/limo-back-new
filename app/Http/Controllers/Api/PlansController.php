<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Plan;
use App\Models\UserPlan;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use function PHPUnit\Framework\isNull;

class PlansController extends Controller
{

    public function myPlans(Request $request)
    {
        if (!$user = auth('api')->user()) {
            return response()->json(['error' => 'Unauthorized'], 401);
        }

//        return response()->json(['success' => false , 'data' => UserPlan::where('user_id', auth('api')->id())->count()], 200);
        if (UserPlan::where('user_id', auth('api')->id())->count()){
            return response()->json(['success' => true, 'data' =>  UserPlan::where('user_id', auth('api')->id())->first() ]);
        }else{
            return response()->json(['success' => false, 'error' => 'have not any active plan yet']);
        }

    }

    public function list()
    {
        $plans = Plan::all();

        return response()->json(['success' => true, 'data' => $plans]);
    }

    public function show(Plan $id)
    {
        return response()->json(['success' => true, 'data' => $id]);
    }


}
