<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Controllers\zarinpal;
use App\Models\Payment;
use App\Models\Plan;

//use http\Env\Response;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Http;


class PaymentController extends Controller
{
    public function purchasePlan(Request $request)
    {


        if (!$user = auth('api')->user()) {
            return response()->json(['success' => false, 'error' => 'Unauthorized'], 401);
        }

        if (!$request->plan_id || empty($request->plan_id)) {
            return response()->json(['success' => false, 'error' => 'plan_id required'], 400);
        }


        $plan = Plan::where('id', $request->plan_id)->firstOrFail();

//        return response()->json($plan);

        $bank_result = $this->createPaymentLink($plan->price);


        Payment::create([
            'user_id' => $user->id,
            'plan_id' => $request->plan_id,
            'price' => $plan->price,
            'gateway' => 'sandbox',
            'authority' => $bank_result['Authority']

        ]);

        return response()->json(['success' => true, 'data' => $bank_result]);

    }


    public function verifyPayment(Request $request)
    {

//        return response()->json($request->all());

        #todo1 get payment buy autorizity

        $payment = Payment::where('Authority', $request->Authority)->firstOrFail();


        #todo2 validate user

        if (!$user = auth('api')->user()) {
            return response()->json(['success' => false, 'error' => 'Unauthorized'], 401);
        }


        if ($payment->user_id != $user->id) return response()->json(['success' => false, 'error' => 'Unauthorized'], 403);


        #todo3 check payment is success

        $verify = $this->verify($payment->price, $request->Authority);


        #todo4 change payment status


        if (isset($verify["Status"]) && $verify["Status"] == 100) {
            $payment->status = 1;
            $payment->save();


            $mutable = Carbon::now();

            $modifiedMutable = $mutable->add($payment->plan->expire_day, 'day');
            DB::table('user_plans')->insert(['user_id' => $user->id,
                'plan_id' => $payment->plan->id,
                'expire' => $modifiedMutable ,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now()
            ]);

            return response()->json(['success' => true, 'data' => $verify]);
        } else {
            return response()->json(['success' => false, 'error' => $verify], 400);
        }


        #todo5 active user plan


//        $payments = Payment::where('authority', $request->Authority)->firstOrFail();
//        dd($payments);


    }


    protected function createPaymentLink($plan_price)
    {
        $MerchantID = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
        $Amount = $plan_price;
        $Description = "تراکنش زرین پال";
        $Email = "";
        $Mobile = "";
        $CallbackURL = env("Front_URI") . '/auth/dashboard/verify';

        $ZarinGate = true;
        $SandBox = true;

        $zp = new zarinpal();

        $result = $zp->request($MerchantID, $Amount, $Description, $Email, $Mobile, $CallbackURL, $SandBox, $ZarinGate);


        return $result;
    }


    protected function verify($price, $authority)
    {
        $MerchantID = "xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx";
        $Amount = $price;
        $ZarinGate = true;
        $SandBox = true;

        $zp = new zarinpal();
        $result = $zp->verify($MerchantID, $Amount, $authority, $SandBox, $ZarinGate);

        return $result;

    }


    protected function getUser($token)
    {
        $user_service_url = env("USER_SERVICE_URL") . "/api/v1/auth/me";
        $user_response = Http::withHeaders([
            'Authorization' => 'Bearer ' . $token
        ])->get($user_service_url);


        if (!$user_response->ok()) return response()->json(['success' => false, 'error' => 'invalid token'], 400);

        return $user_response->json();
    }


    protected function getPlan($plan_id)
    {

        $plan_service_url = env("PLAN_SERVICE_URL") . "/api/v1/plans/" . $plan_id . "/show";


        $plan_response = Http::get($plan_service_url);


        if (!$plan_response->ok()) return response()->json(['success' => false, 'error' => 'invalid plan id'], 400);

        return $plan_response->json();
    }
}
